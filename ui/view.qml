import QtQuick 1.0
import QtWebKit 1.0


WebView {
	javaScriptWindowObjects: QtObject {
		WebView.windowObjectName: "Qt"
		objectName: "window.Qt"
		signal send(string message)
		signal receive(string message)
	}
	url: 'view.html'
	settings.developerExtrasEnabled: true
	width: 800
	height: 480
}