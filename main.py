import json as _json
import logging as _logging
import sys as _sys
import time as _time
import itertools

from PySide import (
	QtCore as _QtCore,
	QtGui as _QtGui,
	QtWebKit as _QtWebKit,
	QtDeclarative as _QtDeclarative
)

_logging.basicConfig(level=_logging.DEBUG)
logger = _logging.getLogger(__name__)

def iterateObjectTree(obj, level=0):
	logger.debug('%s%s: %r' % (' '*level, obj.objectName(), obj))
	yield obj
	for c in obj.children():
		for co in iterateObjectTree(c, level=level+1):
			yield co

def findChildByName(obj, name):
	for c in iterateObjectTree(obj):
		if c.objectName() == name:
			return c

class MainWidget(_QtGui.QWidget):
	send = _QtCore.Signal(str)

	def __init__(self, parent=None):
		super(MainWidget, self).__init__(parent)

		self.layout = _QtGui.QVBoxLayout()
		self.setLayout(self.layout)

		self.textBox = _QtGui.QLineEdit(self)
		self.layout.addWidget(self.textBox)

		qmlView = _QtDeclarative.QDeclarativeView()
		self.layout.addWidget(qmlView)
		qml_url = _QtCore.QUrl('ui/view.qml')
		qmlView.setSource(qml_url)
		qmlView.setResizeMode(_QtDeclarative.QDeclarativeView.SizeRootObjectToView)
		self.webView = qmlView.rootObject()
		self.connectJsSignals(findChildByName(self.webView, 'window.Qt'))

	def sendData(self, data):
		logger.debug('Sending data: %r', data)
		json_str = _json.dumps(data)
		self.send.emit(json_str)

	@_QtCore.Slot(str)
	def receive(self, json_str):
		logger.debug('received: %r', json_str)
		self.textBox.setText(json_str)
		data = _json.loads(json_str)
		self.sendData(['py time', _time.time() * 1000])

	def connectJsSignals(self, obj):
		obj.send.connect(self.receive)
		self.send.connect(obj.receive)

class MyApp(_QtGui.QApplication):
	def __init__(self, args):
		super(MyApp, self).__init__(args)
		self.widget = MainWidget()
		self.widget.show()

if __name__ == '__main__':
	app = MyApp(_sys.argv)
	_sys.exit(app.exec_())
